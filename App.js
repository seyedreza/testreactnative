import React from 'react';
import { Provider } from 'react-redux';
import store from './src/store/store';
import { SafeAreaView } from 'react-navigation';
import { StyleSheet, YellowBox } from 'react-native';
import AppContainer from './src/navigator/AppContainer';

YellowBox.ignoreWarnings(['Remote debugger']);

const App = () => {
  return (
    <Provider store={store}>
      <SafeAreaView style={styles.container} >
        <AppContainer />
      </SafeAreaView>
    </Provider>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

export default App;
