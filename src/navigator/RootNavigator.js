import { createStackNavigator } from 'react-navigation-stack';
import TaskListScreen from '../screens/TaskList';
import NewTaskScreen from '../screens/NewTask';
import SelectLocationScreen from '../screens/SelectLocation';

export default createStackNavigator(
    {
        TaskList: TaskListScreen,
        NewTask: NewTaskScreen,
        SelectLocation: SelectLocationScreen
    },
    {
        initialRouteName: 'TaskList',
        headerMode: 'none'
    }
);