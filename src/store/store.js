import { createStore, applyMiddleware, compose } from 'redux';
import reducers from './reducers';

const middleware = compose(applyMiddleware())

let store = createStore(reducers, {}, middleware)

export default store
