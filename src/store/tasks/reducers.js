import { ADD_TASK, UPDATE_TASK } from './';

const initialState = {
  tasks: []
};

const tasks = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TASK: {
      return {
        ...state,
        tasks: state.tasks.concat([action.payload])
      }
    }
    case UPDATE_TASK: {
      let tasks = state.tasks;
      let index = tasks.findIndex(item => item.id === action.payload.id);
      if (index > -1) {
        tasks[index].location = action.payload.location
      }
      return {...state, tasks: [...tasks]}
    }
    default: {
      return state
    }
  }
}

export default tasks
