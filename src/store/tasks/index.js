export const ADD_TASK = 'ADD_TASK';
export const UPDATE_TASK = 'UPDATE_TASK';

export const addTask = task => ({
  type: ADD_TASK,
  payload: task
});

export const updateTask = data => ({
  type: UPDATE_TASK,
  payload: data
});