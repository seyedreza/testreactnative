import { combineReducers } from 'redux';
import tasks from './tasks/reducers';

const appReducers = combineReducers({
  tasks
});

const app = (state, action) => {
  return appReducers(state, action)
};

const reducers = combineReducers({
  app
});

export default reducers;
