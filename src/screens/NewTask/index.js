import React, { useMemo, useState, useCallback } from 'react';
import { View, Text, ScrollView, TextInput, StyleSheet, TouchableOpacity } from 'react-native';
import Header from '../../components/Header';
import Radio from '../../components/Radio';
import { mainColor } from '../../config/globals';
import { addTask } from '../../store/tasks';
import { useDispatch } from 'react-redux';

const NewTask = props => {

    const radioOptions = useMemo(() => [
        { key: 'done', value: 'Done' },
        { key: 'doing', value: 'Doing' },
        { key: 'todo', value: 'Todo' },
    ], []);

    const dispatch = useDispatch();

    const [name, setName] = useState('');

    const [description, setDescription] = useState('');

    const [status, setStatus] = useState('');

    const handleSelectStatus = useCallback((data) => setStatus(data.value), [status]);

    const handleChangeName = useCallback(text => setName(text), [name]);

    const handleChangeDesc = useCallback(text => setDescription(text), [description]);

    const validForm = () => name.length && description.length && status.length;

    const handleConfirm = () => {
        let id = Math.random().toString();
        dispatch(addTask({ id, name, description, status }));
        props.navigation.navigate('SelectLocation', {
            taskId: id
        });
    };

    return (
        <View style={styles.container}>
            <Header title='Add New Task' goBack />
            <ScrollView style={styles.containerForm}>
                <TextInput
                    placeholder='Name *'
                    style={styles.taskName}
                    returnKeyType='done'
                    autoCapitalize='none'
                    onChangeText={handleChangeName}
                    value={name}
                />

                <TextInput
                    placeholder='Description *'
                    style={styles.taskDescription}
                    returnKeyType='done'
                    autoCapitalize='none'
                    multiline
                    numberOfLines={5}
                    onChangeText={handleChangeDesc}
                    value={description}
                />
                <View style={styles.containerStatus}>
                    <Radio options={radioOptions} onSelect={handleSelectStatus} label='Task Status:' />
                </View>
                <TouchableOpacity
                    style={[styles.nextStep, { backgroundColor: !validForm() ? '#e8e8e8' : mainColor }]}
                    disabled={!validForm()}
                    onPress={handleConfirm}>
                    <Text>Next Step</Text>
                </TouchableOpacity>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    containerForm: {
        paddingTop: 50,
        width: '90%',
        alignSelf: 'center',
    },
    taskName: {
        borderWidth: 1,
        borderColor: 'gray',
        paddingLeft: 15,
        borderRadius: 5,
    },
    taskDescription: {
        borderWidth: 1,
        borderColor: 'gray',
        paddingLeft: 15,
        borderRadius: 5,
        marginTop: 50,
        textAlignVertical: 'top'
    },
    containerStatus: {
        marginTop: 50,
    },
    nextStep: {
        borderWidth: 1,
        borderColor: 'black',
        borderRadius: 5,
        paddingTop: 10,
        paddingBottom: 10,
        paddingRight: 20,
        paddingLeft: 20,
        width: '50%',
        alignSelf: 'center',
        alignItems: 'center',
        marginTop: '30%',
    }
});

export default NewTask;