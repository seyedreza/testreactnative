import React, { useCallback } from 'react';
import {
    View,
    FlatList,
    Text,
    TouchableOpacity,
    Dimensions,
    StyleSheet
} from 'react-native';
import Header from '../../components/Header';
import { headerHeight, mainColor } from '../../config/globals';
import { useSelector } from 'react-redux';

const screenHeight = Dimensions.get("window").height;

const TaskList = props => {

    const tasks = useSelector(state => state.app.tasks.tasks);

    const keyExtractor = item => item.id;

    const renderItem = ({ item }) => {
        return (
            <View style={styles.item}>
                <Text>{item.name}</Text>
                <Text>{item.description}</Text>
                <Text>{item.status}</Text>
                <Text>{item.location}</Text>
            </View>
        )
    };

    const renderHeader = () => <Header title='Hello world' />;

    const handleNewTask = useCallback(() => props.navigation.navigate('NewTask'), []);

    const renderEmpty = () => (
        <View style={styles.emptyContainer}>
            <TouchableOpacity onPress={handleNewTask} style={styles.emptyBtnNewTask}>
                <Text>Add New Task</Text>
            </TouchableOpacity>
        </View>
    );

    const renderNewTaskBTN = () => {
        if (tasks.length) {
            return (
                <TouchableOpacity style={styles.floating} onPress={handleNewTask}>
                    <Text style={styles.textFloating}>+</Text>
                </TouchableOpacity>
            )
        }
        return null
    }

    const renderSeparator = () => <View style={styles.separator} />

    return (
        <>
            <FlatList
                data={tasks}
                keyExtractor={keyExtractor}
                renderItem={renderItem}
                ListHeaderComponent={renderHeader}
                ListEmptyComponent={renderEmpty}
                showsVerticalScrollIndicator={false}
                stickyHeaderIndices={[0]}
                ItemSeparatorComponent={renderSeparator}
            />
            {renderNewTaskBTN()}
        </>
    );
}

const styles = StyleSheet.create({
    emptyContainer: {
        height: screenHeight - headerHeight,
        justifyContent: 'center',
        alignItems: 'center'
    },
    emptyBtnNewTask: {
        borderWidth: 1,
        borderColor: 'black',
        borderRadius: 5,
        backgroundColor: mainColor,
        paddingTop: 10,
        paddingBottom: 10,
        paddingRight: 20,
        paddingLeft: 20
    },
    floating: {
        position: 'absolute',
        width: 55,
        height: 55,
        backgroundColor: mainColor,
        borderRadius: 30,
        bottom: 10,
        right: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textFloating: {
        fontSize: 25
    },
    item: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 20,
        paddingBottom: 20,
        paddingLeft: 10,
        paddingRight: 10,
        flexWrap: 'wrap'
    },
    separator: {
        borderTopWidth: 1,
        borderTopColor: mainColor
    }
});

export default TaskList;