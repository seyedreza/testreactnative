import React, { useState } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import Header from '../../components/Header';
import MapView from 'react-native-maps';
import { mainColor } from '../../config/globals';
import { useDispatch } from 'react-redux';
import { updateTask } from '../../store/tasks';

const width = Dimensions.get('window').width;

const SelectLocation = props => {
    
    const taskId = props.navigation.getParam('taskId', '');

    const dispatch = useDispatch();

    const [location, setLocation] = useState({ latitude: 35.724052, longitude: 51.3937 });

    const handleConfirm = () => {
        dispatch(updateTask({ id: taskId, location: `${Number((location.latitude).toFixed(4))},${Number((location.longitude).toFixed(4))}` }));
        props.navigation.popToTop();
    };

    const onPressMap = region => {
        setLocation({
            latitude: region.nativeEvent.coordinate.latitude,
            longitude: region.nativeEvent.coordinate.longitude
        });
    };

    return (
        <View style={styles.container}>
            <Header title='Select Location' goBack />
            <View style={styles.containerMap}>
                <MapView
                    style={styles.map}
                    region={{
                        ...location,
                        latitudeDelta: 0.015,
                        longitudeDelta: 0.0121,
                    }}
                    onPress={onPressMap} >
                    <MapView.Marker coordinate={location} />
                </MapView>
            </View>
            <TouchableOpacity style={styles.nextStep} onPress={handleConfirm}>
                <Text>Next Step</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    containerMap: {
        flex: 1
    },
    map: {
        height: '100%',
        width: width,
    },
    nextStep: {
        borderWidth: 1,
        borderColor: 'black',
        borderRadius: 5,
        paddingTop: 10,
        paddingBottom: 10,
        paddingRight: 20,
        paddingLeft: 20,
        width: '50%',
        alignSelf: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: 20,
        backgroundColor: mainColor
    }
});

export default SelectLocation;