import React, { memo, useCallback } from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { headerHeight } from '../../config/globals';
import { withNavigation } from 'react-navigation';
import IconMaterial from 'react-native-vector-icons/MaterialIcons';

const Header = props => {

    const { title, goBack = false } = props;

    const handleBack = useCallback(() => props.navigation.goBack(), []);

    return (
        <View style={styles.container}>
            <View style={styles.back}>
                {
                    goBack ? (
                        <TouchableOpacity onPress={handleBack}>
                            <IconMaterial name='keyboard-backspace' size={20} />
                        </TouchableOpacity>
                    ) : null
                }
            </View>
            <View style={styles.title}><Text>{title}</Text></View>
            <View style={styles.empty} />
        </View>
    );
};

Header.propTypes = {
    title: PropTypes.string.isRequired,
    goBack: PropTypes.bool
};

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        height: headerHeight,
        flexDirection: 'row',
        backgroundColor: '#e8e8e8',
        paddingLeft: 10
    },
    back: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    title: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    empty: {
        flex: 1,
    }
});

export default withNavigation(memo(Header));