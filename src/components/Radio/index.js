import React, { useState, memo, useCallback } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { mainColor } from '../../config/globals';

const Radio = props => {

    const { options, onSelect, label = '' } = props;

    const [value, setValue] = useState(undefined);

    const handleSelect = useCallback((item) => {
        setValue(item.key);
        if (onSelect) {
            onSelect(item);
        }
    }, []);

    return (
        <View>
            <Text>{label}</Text>
            <View style={styles.container}>
                {
                    options.map(item => {
                        return (
                            <TouchableOpacity onPress={() => handleSelect(item)} key={item.key} style={styles.buttonContainer}>
                                <View style={styles.circle}>
                                    {value === item.key && (<View style={styles.checkedCircle} />)}
                                </View>
                                <Text>{item.value}</Text>
                            </TouchableOpacity>
                        )
                    })
                }
            </View>
        </View>
    )
}

Radio.propTypes = {
    options: PropTypes.arrayOf(PropTypes.shape({
        key: PropTypes.oneOfType[PropTypes.string, PropTypes.number],
        value: PropTypes.oneOfType[PropTypes.string, PropTypes.number]
    })),
    onSelect: PropTypes.func,
    label: PropTypes.string
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 10,
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    circle: {
        height: 30,
        width: 30,
        borderRadius: 20,
        borderWidth: 1,
        borderColor: '#ACACAC',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 5
    },
    checkedCircle: {
        width: 20,
        height: 20,
        borderRadius: 10,
        backgroundColor: mainColor,
    },
})

export default memo(Radio)